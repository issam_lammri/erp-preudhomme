<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * ClientRepository is a class method which is used to build queries in a centralized file
 * The ClientRepository class extend the ServiceEntityRepository class
 * @package    src/Repository
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/doctrine.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class ClientRepository extends ServiceEntityRepository
{
    /**
     * construct for ClientRepository to intialize it
     * ClientRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * function to update PayRoll a company if when all employees gets paid
     * @param float $salaire
     * @param int $entreprise
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function updatePayRollClient(float $salaire, int $entreprise)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            UPDATE client c 
            SET c.payroll = (
                c.payroll - ( 
                    select (count(em.id) * :salaire ) as countEmpl 
                    from employer em 
                    where em.entreprise = :entreprise )) 
            where c.id = :entreprise 
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['salaire' => $salaire, 'entreprise' => $entreprise]);
    }

    /**
     * function to update PayRoll a company if when one employee gets paid
     * @param float $salaire
     * @param int $entreprise
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function updatePayRollClientOneSalary(float $salaire, int $entreprise)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            UPDATE client c 
            SET c.payroll = c.payroll - :salaire
            where c.id = :entreprise 
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['salaire' => $salaire, 'entreprise' => $entreprise]);
    }
}
