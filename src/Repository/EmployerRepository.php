<?php

namespace App\Repository;

use App\Entity\Employer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * EmployerRepository is a class method which is used to build queries in a centralized file
 * The EmployerRepository class extend the ServiceEntityRepository class
 * @package    src/Repository
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/doctrine.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class EmployerRepository extends ServiceEntityRepository
{
    /**
     * construct for EmployerRepository to intialize it
     * ClientRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employer::class);
    }

    /**
     * function to update salary for all employees if they gets paid by the company
     * @param float $salaire
     * @param int $entreprise
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateSalariesEmployes(float $salaire, int $entreprise)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            UPDATE employer 
            SET salary = salary +  :salaire
            WHERE entreprise = :entreprise
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['salaire' => $salaire, 'entreprise' => $entreprise]);
    }

    /**
     * function to update salary for one employee if he gets paid by the company
     * @param float $salaire
     * @param int $id_employe
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateSalariesOneEmploye(float $salaire, int $id_employe)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            UPDATE employer 
            SET salary = salary +  :salaire
            WHERE id = :id_employe
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['salaire' => $salaire, 'id_employe' => $id_employe]);
    }
}
