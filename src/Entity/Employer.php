<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Employer is a class used to create a Employer object
 * @package     src/Entity
 * @author      DIB, LAMMRI, HAFID, KEBBABI
 * @version     1.0
 * @access      public
 * @link        http://www.phpdoc.org
 * @link        http://pear.php.net/PhpDocumentor
 * @ORM\Table(name="employer", indexes={@ORM\Index(name="entreprise", columns={"entreprise"})})
 * @ORM\Entity
 */
class Employer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firsname", type="string", length=255, nullable=false)
     */
    private $firsname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
     */
    private $lastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdayDate", type="date", nullable=false)
     */
    private $birthdaydate;

    /**
     * @var float
     *
     * @ORM\Column(name="salary", type="float", precision=10, scale=0, nullable=false)
     */
    private $salary;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entreprise", referencedColumnName="id")
     * })
     */
    private $entreprise;

    /**
     * Get the id of this employer
     * @return int|id Int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the firstname of this employer
     * @return string|firsname String
     */
    public function getFirsname(): ?string
    {
        return $this->firsname;
    }

    /**
     * Edit the value of employer firstname
     * @param string $firsname
     * @return $this
     */
    public function setFirsname(string $firsname): self
    {
        $this->firsname = $firsname;

        return $this;
    }

    /**
     * Get the Lastname of this employer
     * @return string|lastname String
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * Edit the value of employer lastname
     * @param string $lastname
     * @return $this
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the birthday date of this employer
     * @return \DateTimeInterface|birthdaydate DateTimeInterface
     */
    public function getBirthdaydate(): ?\DateTimeInterface
    {
        return $this->birthdaydate;
    }

    /**
     * Edit the value of employer birthday date
     * @param \DateTimeInterface $birthdaydate
     * @return $this
     */
    public function setBirthdaydate(\DateTimeInterface $birthdaydate): self
    {
        $this->birthdaydate = $birthdaydate;

        return $this;
    }

    /**
     * Get the salary of this employer
     * @return float|salary Float
     */
    public function getSalary(): ?float
    {
        return $this->salary;
    }

    /**
     * Edit the value of employer salary
     * @param float $salary
     * @return $this
     */
    public function setSalary(float $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get the company of this employer
     * @return Client Client
     */
    public function getEntreprise(): Client
    {
        return $this->entreprise;
    }

    /**
     * Edit the value of employer company
     * @param Client|null $entreprise
     * @return $this
     */
    public function setEntreprise(?Client $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getFirsname() . " " . (string)$this->getLastname();
    }
}
