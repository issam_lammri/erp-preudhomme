<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client is a class used to create a client object
 * @package     src/Entity
 * @author      DIB, LAMMRI, HAFID, KEBBABI
 * @version     1.0
 * @access      public
 * @link        http://www.phpdoc.org
 * @link        http://pear.php.net/PhpDocumentor
 * @ORM\Table(name="client")
 * @ORM\Entity
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="turnover", type="float", precision=10, scale=0, nullable=false)
     */
    private $turnover;

    /**
     * @var float
     *
     * @ORM\Column(name="payroll", type="float", precision=10, scale=0, nullable=false)
     */
    private $payroll;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var int
     *
     * @ORM\Column(name="codePostal", type="integer", nullable=false)
     */
    private $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=false)
     */
    private $pays;

    /**
     * Get the id of this client
     * @return int|id Int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the name of this client
     * @return string|name String
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Edit the value of client name
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the turnover of this client
     * @return float|turnover Float
     */
    public function getTurnover(): ?float
    {
        return $this->turnover;
    }

    /**
     * Edit the value of client turnover
     * @param float $turnover
     * @return $this
     */
    public function setTurnover(float $turnover): self
    {
        $this->turnover = $turnover;

        return $this;
    }

    /**
     * Get the payroll of this client
     * @return float|payroll Float
     */
    public function getPayroll(): ?float
    {
        return $this->payroll;
    }

    /**
     * Edit the value of client payroll
     * @param float $payroll
     * @return $this
     */
    public function setPayroll(float $payroll): self
    {
        $this->payroll = $payroll;

        return $this;
    }

    /**
     * Get the address of this client
     * @return string|address String
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Edit the value of client address
     * @param string $address
     * @return $this
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the postal code of this client
     * @return int|codepostal Int
     */
    public function getCodepostal(): ?int
    {
        return $this->codepostal;
    }

    /**
     * Edit the value of client postal code
     * @param int $codepostal Postal code
     * @return $this
     */
    public function setCodepostal(int $codepostal): self
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get the city of this client
     * @return string|ville String
     */
    public function getVille(): ?string
    {
        return $this->ville;
    }

    /**
     * Edit the value of client city
     * @param string $ville City
     * @return $this
     */
    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get the country of this client
     * @return string|pays String
     */
    public function getPays(): ?string
    {
        return $this->pays;
    }

    /**
     * Edit the value of client country
     * @param string $pays Country
     * @return $this
     */
    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Return the string format of this client name
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getName();
    }
}
