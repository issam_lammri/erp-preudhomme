<?php

namespace App\Entity;

use App\Repository\DemoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Demo is a class used to create a Demo object
 * @package     src/Entity
 * @author      DIB, LAMMRI, HAFID, KEBBABI
 * @version     1.0
 * @access      public
 * @link        http://www.phpdoc.org
 * @link        http://pear.php.net/PhpDocumentor
 * @ORM\Entity(repositoryClass=DemoRepository::class)
 */
class Demo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $demo;

    /**
     * Get the id of this demo
     * @return int|id Int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the id of this demo
     * @return string|demo String
     */
    public function getDemo(): ?string
    {
        return $this->demo;
    }

    /**
     * Edit the value of demo name
     * @param string $demo
     * @return $this
     */
    public function setDemo(string $demo): self
    {
        $this->demo = $demo;

        return $this;
    }
}
