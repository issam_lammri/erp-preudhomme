<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * DashboardController is a class method which is used to accept requests, and return a Response object.
 * The DashboardController class extend the AbstractController class
 * @package    src/Controller
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/controller.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class DashboardController extends AbstractController
{
    /**
     * function that allows to redirect us to the homePage
     * @Route("/", name="dashboard")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('home');
    }
}
