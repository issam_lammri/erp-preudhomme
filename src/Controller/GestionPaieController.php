<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Form\GestionPaieEmployeType;
use App\Form\GestionPaieType;
use App\Repository\ClientRepository;
use App\Repository\EmployerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * GestionPaieController is a class method which is used to accept requests, and return a Response object.
 * The GestionPaieController class extend the AbstractController class
 * @package    src/Controller
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/controller.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 * @Route("/paie")
 */
class GestionPaieController extends AbstractController
{
    /**
     * page that allow to send a salary to all employees for a specific company
     * @Route("/", name="gestion_paie", methods={"GET","POST"})
     * @param Request $request
     * @param EmployerRepository $employerRepository
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function index(
        Request $request,
        EmployerRepository $employerRepository,
        ClientRepository $clientRepository
    ): Response {
        $form = $this->createForm(GestionPaieType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $salaire = $request->request->get('gestion_paie')['salaire'];
            $entreprise = $request->request->get('gestion_paie')['Client'];
            $employerRepository->updateSalariesEmployes($salaire, $entreprise);
            $clientRepository->updatePayRollClient($salaire, $entreprise);
            $this->addFlash('success', 'Les salaires ont été bien mis à jour!');
            return $this->redirectToRoute('gestion_paie');
        }

        return $this->render('GestionPaie/gestionPaie.html.twig', [
            'controller_name' => 'GESTION PAIE',
            'form' => $form->createView(),
        ]);
    }


    /**
     * page that allow to send a salary to a specific employee in a specific company
     * @Route("/employe", name="gestionPaieEmploye", methods={"GET","POST"})
     * @param Request $request
     * @param EmployerRepository $employerRepository
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function gestionPaieEmploye(
        Request $request,
        EmployerRepository $employerRepository,
        ClientRepository $clientRepository
    ): Response {
        $clients = $this->getDoctrine()
            ->getRepository(Client::class)
            ->findAll();

        if (isset($request->request->get('gestionPaieEmploye')['Payer'])) {
            $client_id = $request->request->get('gestionPaieEmploye')['Client'];
            $employe_id = $request->request->get('gestionPaieEmploye')['Employe'];
            $salaire = $request->request->get('gestionPaieEmploye')['salaire'];
            $employerRepository->updateSalariesOneEmploye($salaire, $employe_id);
            $clientRepository->updatePayRollClientOneSalary($salaire, $client_id);
            $this->addFlash('success', 'Le Salaire de cette Employé a été bien mis à jour!');
        }

        return $this->render('GestionPaie/gestionPaieEmploye.html.twig', [
            'controller_name' => 'GESTION PAIE POUR UN SALARIE',
            'clients' => $clients,
        ]);
    }
}
