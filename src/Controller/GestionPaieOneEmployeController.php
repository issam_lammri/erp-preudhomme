<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Form\GestionPaieEmployeType;
use App\Form\GestionPaieType;
use App\Repository\ClientRepository;
use App\Repository\EmployerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * GestionPaieOneEmployeController is a class method which is used to accept requests, and return a Response object.
 * The GestionPaieOneEmployeController class extend the AbstractController class
 * @package    src/Controller
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/controller.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 * @Route("/paieOneEmploye")
 */
class GestionPaieOneEmployeController extends AbstractController
{
    /**
     * page that allow to send a salary to a specific employee in a specific company
     * @Route("/", name="gestionPaieEmploye", methods={"GET","POST"})
     * @param Request $request
     * @param EmployerRepository $employerRepository
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function gestionPaieEmploye(
        Request $request,
        EmployerRepository $employerRepository,
        ClientRepository $clientRepository
    ): Response {
        $clients = $this->getDoctrine()
            ->getRepository(Client::class)
            ->findAll();

        if (isset($request->request->get('gestionPaieEmploye')['Payer'])) {
            $client_id = $request->request->get('gestionPaieEmploye')['Client'];
            $employe_id = $request->request->get('gestionPaieEmploye')['Employe'];
            $salaire = $request->request->get('gestionPaieEmploye')['salaire'];
            $employerRepository->updateSalariesOneEmploye($salaire, $employe_id);
            $clientRepository->updatePayRollClientOneSalary($salaire, $client_id);
            $this->addFlash('success', 'Le Salaire de cette Employé a été bien mis à jour!');
        }

        return $this->render('GestionPaie/gestionPaieEmploye.html.twig', [
            'controller_name' => 'GESTION PAIE POUR UN SALARIE',
            'clients' => $clients,
        ]);
    }
}
