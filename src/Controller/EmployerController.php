<?php

namespace App\Controller;

use App\Entity\Employer;
use App\Form\EmployerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * EmployerController is a class method which is used to accept requests, and return a Response object.
 * The EmployerController class extend the AbstractController class
 * @package    src/Controller
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/controller.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 * @Route("/employer")
 */
class EmployerController extends AbstractController
{
    /**
     * page that displays all employees
     * @Route("/", name="employer_index", methods={"GET"})
     */
    public function index(): Response
    {
        $employers = $this->getDoctrine()
            ->getRepository(Employer::class)
            ->findAll();

        return $this->render('employer/index.html.twig', [
            'employers' => $employers,
        ]);
    }

    /**
     * function to get all employees by client id
     * @Route("/employeByClient", name="employer_by_cient", methods={"POST"})
     */
    public function employeByClient(Request $request): JsonResponse
    {

        $idClient = $request->request->get('id');
        $employers = $this->getDoctrine()
            ->getRepository(Employer::class)
            ->findByEntreprise($idClient);

        if ($employers != null) {
            foreach ($employers as $employe) {
                $output[] = array("id" => $employe->getId(),
                    "NameClient" => $employe->getFirsname() . " " . $employe->getLastname());
            }
            return new JsonResponse($output);
        } else {
            return new JsonResponse();
        }
    }

    /**
     * page that allows to create a new employee
     * @Route("/new", name="employer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $employer = new Employer();
        $form = $this->createForm(EmployerType::class, $employer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($employer);
            $entityManager->flush();

            return $this->redirectToRoute('employer_index');
        }

        return $this->render('employer/new.html.twig', [
            'employer' => $employer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * page that displays information about one employee
     * @Route("/{id}", name="employer_show", methods={"GET"})
     */
    public function show(Employer $employer): Response
    {
        return $this->render('employer/show.html.twig', [
            'employer' => $employer,
        ]);
    }

    /**
     * page that allows to edit information about one employee
     * @Route("/{id}/edit", name="employer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Employer $employer): Response
    {
        $form = $this->createForm(EmployerType::class, $employer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employer_index');
        }

        return $this->render('employer/edit.html.twig', [
            'employer' => $employer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * page that allows to delete one employee
     * @Route("/{id}", name="employer_delete", methods={"POST"})
     */
    public function delete(Request $request, Employer $employer): Response
    {
        if ($this->isCsrfTokenValid('delete' . $employer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($employer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('employer_index');
    }
}
