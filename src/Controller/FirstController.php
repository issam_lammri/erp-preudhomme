<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * FirstController is a class method which is used to accept requests, and return a Response object.
 * The FirstController class extend the AbstractController class
 * @package    src/Controller
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/controller.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class FirstController extends AbstractController
{
    /**
     * homepage to display all information about the developers
     * @Route("/home", name="home")
     */
    public function index(): Response
    {
        return $this->render('first/index.html.twig', [
            'controller_name' => 'FirstController',
        ]);
    }
}
