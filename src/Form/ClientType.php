<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ClientType is a class method which is used to create a form that it will be used in the interface (twig)
 * The ClientType class extend the AbstractType class
 * @package    src/Form
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/forms.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class ClientType extends AbstractType
{
    /**
     * function to build a form using parameters or inputs that we use
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('turnover')
            ->add('payroll')
            ->add('address')
            ->add('codepostal')
            ->add('ville')
            ->add('pays')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
