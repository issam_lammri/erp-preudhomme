<?php

namespace App\Form;

use App\Entity\Employer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * EmployerType is a class method which is used to create a form that it will be used in the interface (twig)
 * The EmployerType class extend the AbstractType class
 * @package    src/Form
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/forms.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class EmployerType extends AbstractType
{
    /**
     * function to build a form using parameters or inputs that we use
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firsname')
            ->add('lastname')
            ->add('birthdaydate')
            ->add('salary')
            ->add('entreprise')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employer::class,
        ]);
    }
}
