<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Employer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GestionPaieEmployeType is a class method which is used to create a form that it will be used in the interface (twig)
 * The GestionPaieEmployeType class extend the AbstractType class
 * @package    src/Form
 * @author     DIB, LAMMRI, HAFID, KEBBABI
 * @version    1.0
 * @access     public
 * @see        https://symfony.com/doc/current/forms.html
 * @link       http://www.phpdoc.org
 * @link       http://pear.php.net/PhpDocumentor
 */
class GestionPaieEmployeType extends AbstractType
{
    /**
     * function to build a form using parameters or inputs that we use
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Client', EntityType::class, [
                'class' => Client::class,
                'label' => 'Name Client : ',
                'mapped' => false,
                'required' => true,
                'attr' => ['class' => 'form-control']
            ])
            ->add('Employe', EntityType::class, [
                'class' => Employer::class,
                'label' => 'Employe :  ',
                'required' => true,
                'mapped' => false,
                'empty_data' => true,
                'attr' => ['class' => 'form-control']
            ])
            ->add('salaire', IntegerType::class, [
                'label' => 'Salaire :  ',
                'required' => true,
                'mapped' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('Payer', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
            'csrf_token_id' => 'gestion_paie_csrf'
        ]);
    }
}
