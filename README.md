"# MSPR 3 Preudhomme" 

recupérer le projet : 

    git clone https://gitlab.com/issam_lammri/erp-preudhomme.git

installer les dépendances 

    composer install 

Installing Panther
Use Composer to install Panther in your project. You may want to use the --dev flag if you want to use Panther for testing only and not for web scraping in a production environment:

    composer req symfony/panther

    composer req --dev symfony/panther

Lancez les tests pour vérifier qu’ils passent :

    symfony php bin/phpunit

Lancer les containers docker:

    cd docker
    docker-compose up -d

pour accéder à l'invite de commande docker : 

    cd docker
    docker exec -it www_docker_preudhomme bash









