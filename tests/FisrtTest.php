<?php

namespace App\Tests;

use App\Entity\Demo;
use PHPUnit\Framework\TestCase;

class FisrtTest extends TestCase
{
    public function testDemo(): void
    {
        $demo = new Demo();
        $demo->setDemo('youness');

        $this->assertTrue($demo->getDemo() === 'youness');
    }
}
